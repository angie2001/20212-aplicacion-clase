//Importa ExpressJS
const express = require("express");

const tareasRoute = require("./routes/tareas");
const usuariosRoute = require("./routes/usuarios");

//Crea la aplicación
const app = express();

//Permite leer el body  de las peticiones
app.use(express.json());


//Middleware de tareas
app.use("/tareas", tareasRoute);
app.use("/usuarios", usuariosRoute);

//Rutas
app.get("/", (req,res)=>{
    res.send("Lista de tareas");
});

//Ejecuta el servidor
app.listen(3000);
