//Importa ExpressJS
const express = require ("express");

//Crea el router
const router = express.Router();

let listaTareas = [
    {id: 1, nombre:"Running"},
    {id: 2, nombre:"Programming"},
    {id: 3, nombre:"Watch TV"},

]
router.get("/",(req,res)=>{  
    res.send(listaTareas);
})

router.get("/:id",(req,res)=>{
    //Obtiene el id por parámetro
    const id = req.params.id;

    //Filtra las tareas por id
    const tareas = listaTareas.filter(t => t.id == id);
    console.log(tareas);

    //Verifica que se haya encontrado una tarea
    if(tareas.length > 0){
        res.status(200).send(tareas[0]);
    }else{
        res.status(404).send({mensaje: 'No se encontró la tarea'})
    }

})

router.post('/',(req,res)=>{
    //obtener el body
    
    const {id, nombre } = req.body;
    //Crea un objeto de tarea
    const tarea =  {id, nombre };
    //Agrega la tarea a listaTareas
    listaTareas.push(tarea)

    
    res.status(201).send(tarea)
})
router.patch('/:id',(req,res) =>{
    //Obtiene el id
    const id = req.params.id;

    //Obtiene el nuevo nombre
    const {nombre} = req.body;
    console.log(id, nombre);

    let encontrada = false;

    for(let i = 0; i < listaTareas.length; i++){
        //Obtiene la tarea actual
        const tarea = listaTareas[i];

        if(tarea.id == id){
            tarea.nombre = nombre;
            encontrada = true;
        }
    }
    //Dependiendo del valor de respuesta, se modifica o no la tarea
    //si se llega a encontrar en la lista de tareas
    if(encontrada){
        res.status(201).send({mensaje: "La tarea se modificó"});
    }else{
        res.status(404).send({mensaje: "No se encontró la tarea con id "+id})
    }

})

router.delete('/:id',(req,res) =>{
    //Obtiene el id de los parámetros
    const {id} = req.params;

    const tareas = listaTareas.filter(t => t.id == id);
    console.log(tareas);

    //Verifica que se haya encontrado una tarea
    if(tareas.length == 0){
        return res.status(404).send({mensaje: 'No se encontró la tarea'})
    }

    listaTareas = listaTareas.filter(t => t.id != id);
    
    res.status(200).send({mensaje: "Eliminando la tarea..."});
})

module.exports = router;
