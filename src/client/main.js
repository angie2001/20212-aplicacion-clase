// Librerías
const { app, BrowserWindow, Menu, ipcMain } = require("electron");
const url = require("url");
const path = require("path");
 
let ventanaPrincipal;
let ventanaNuevaTarea;

function crearVentanaNuevaTarea(){
    //Crear la ventana para crear tarea
    ventanaNuevaTarea = new BrowserWindow({
        width:400,
        height:300,
        title:"Nueva Tarea",
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
        },
    });
    //Elimina el menu de la ventana
    //ventanaNuevaTarea.setMenu(null);
    //Carga el archivo html en la ventana
    ventanaNuevaTarea.loadURL(
        url.format({
            pathname: path.join(__dirname,"views/nuevaTarea.html"),
            protocol:"file",
            slashes:true,
        })
    );
    //La ventana principal escucha el evento closed
    ventanaNuevaTarea.on("closed",()=>{
    //Libera la memoria reservada para la ventana
        ventanaNuevaTarea=null;
    });
}

function cargarInicio(){
    //Crea la ventana principal
    ventanaPrincipal=new BrowserWindow({
        webPreferences: {
            nodeIntegration: true,
            contextIsolation: false,
        },
    });
    //Carga el archivo HTML en la ventana
    ventanaPrincipal.loadURL(
        url.format({
            pathname: path.join(__dirname,"views/index.html"),
            protocol:"file",
            slashes:true,
        })
    );
    //Construye el menú principal
    const menuPrincipal = Menu.buildFromTemplate(templateMenu);
    Menu.setApplicationMenu(menuPrincipal);
    //La ventana principal escucha el evento closed
    ventanaPrincipal.on("closed", ()=>{
        //Cierra la aplicación 
        app.quit();
    });

    //ipcMAin escucha el evento 'nueva-tarea'
    ipcMain.on("nueva-tarea",(evento,datos) =>{
        
        //Envia el mensaje a la ventan principal
        ventanaPrincipal.webContents.send('nueva-tarea',datos);
        //
        ventanaNuevaTarea.close();


    });
}

//Ejecuta cargarInicio cuando la app está lista
app.on("ready", cargarInicio);

const templateMenu = [
    { 
        label: "Tareas",
        submenu:[
            {
                label:"Nueva tarea",
                acceletaron: "Ctrl+N",
                click(){
                    crearVentanaNuevaTarea();
                },
            },
            {
                label:"Salir",
                accelerator: process.platform ==='darwin' ? "command+Q" : "Ctrl+Q",
                click(){
                    //Cierra la aplicación
                    app.quit();
                }
            },
        ],
    },
];

if(!app.isPackaged){
    templateMenu.push({
        label: "DevTools",
        submenu:[
            {
                label: "Mostrar/Ocultar DevTools",
                click(item,focusedWindow){
                    focusedWindow.toggleDevTools();
                },
            },
            {
                role: "Reload"
            }
        ]
    })

}